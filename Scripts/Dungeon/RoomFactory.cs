using Godot;
using System;
using System.Collections.Generic;

public static class RoomFactory
{
    private static Random rnd = new Random();
    private static Vector2I roomSize = new Vector2I(6,6);

    public static StartRoom CreateStartRoom(Vector2I slot)
    {
        return new StartRoom(roomSize, slot * roomSize);
    }
    public static EndRoom CreateEndRoom(Vector2I slot)
    {
        return new EndRoom(roomSize, slot * roomSize);
    }
    public static RoomBase CreateRandomRoom(Vector2I slot)
    {
        RoomBase newRoom = null;
        switch(rnd.Next(0,2))
        {
            case 0:
                newRoom = CreateChallengeRoom(slot); 
                break;
            case 1:
                newRoom = CreateTreasureRoom(slot); 
                break;
            case 2:
                newRoom = CreateBattleRoom(slot);
                break;
        }
        return newRoom;
    }

    public static ChallengeRoom CreateChallengeRoom(Vector2I slot)
    {
        return new ChallengeRoom(roomSize, slot * roomSize);
    }
    public static TreasureRoom CreateTreasureRoom(Vector2I slot)
    {
        return new TreasureRoom(roomSize, slot * roomSize);
    }
    public static BattleRoom CreateBattleRoom(Vector2I slot)
    {
        return new BattleRoom(roomSize, slot * roomSize);
    }
}