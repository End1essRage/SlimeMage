using Godot;
using System;
using System.Collections.Generic;

public partial class FloorFactory
{
    private Random rnd = new Random();
    public Floor CreateFloor(FloorSettings settings)
    {
        List<RoomSlot> rooms = new List<RoomSlot>();
        Vector2I lastSlot = new Vector2I(0,0);

        RoomSlot startRoom = new RoomSlot();
        startRoom.room = RoomFactory.CreateStartRoom(lastSlot);
        startRoom.slot = lastSlot;
        rooms.Add(startRoom);

        for(int i = 0; i <= settings.RoomsCount; i++)
        {    
            var curSlot = lastSlot;
            int counter = 0;
            do{
                counter++;
                curSlot = lastSlot;
                int roomDirection = rnd.Next(0, 4);
                switch(roomDirection)
                {
                    case 0:
                        curSlot.X++;
                        break;
                    case 1:
                        curSlot.X--;
                        break;
                    case 2:
                        curSlot.Y++;
                        break;
                    case 3:
                        curSlot.Y--;
                        break;
                }
            }while(rooms.Exists(r => r.slot == curSlot) && counter < 4);            

            if(i == settings.RoomsCount)
            {
                RoomSlot endRoom = new RoomSlot();
                endRoom.room = RoomFactory.CreateEndRoom(curSlot);
                endRoom.slot = curSlot;
                rooms.Add(endRoom);
                break;
            }

            RoomSlot newRoom = new RoomSlot();
            newRoom.room = RoomFactory.CreateRandomRoom(curSlot);
            newRoom.slot = curSlot;
            rooms.Add(newRoom);
            
            lastSlot = curSlot;
        }

        AddDoors(rooms);
        return new Floor(rooms);
    }
    //TODO refactor
    private void AddDoors(List<RoomSlot> rooms)
    {
        foreach(var room in rooms)
        {
            var connected = rooms.FindAll(c => c.slot.X == room.slot.X && (c.slot.Y - room.slot.Y == -1 || c.slot.Y - room.slot.Y == 1));
            connected.AddRange(rooms.FindAll(c => c.slot.Y == room.slot.Y && (c.slot.X - room.slot.X == -1 || c.slot.X - room.slot.X == 1)));
            foreach(var connection in connected)
            {
                var position = new Vector2I();
                bool isVertical = false; 

                if(room.room.Position.X < connection.room.Position.X)
                {
                    position = new Vector2I(room.room.Position.X + room.room.Size.X, room.room.Position.Y + room.room.Size.Y / 2);
                    isVertical = true;
                }
                else if(room.room.Position.X > connection.room.Position.X)
                {
                    position = new Vector2I(room.room.Position.X, room.room.Position.Y + room.room.Size.Y / 2);
                    isVertical = true;
                }
                else if(room.room.Position.Y < connection.room.Position.Y)
                {
                    position = new Vector2I(room.room.Position.X + room.room.Size.X / 2, room.room.Position.Y + room.room.Size.Y);
                    isVertical = false;
                }
                else if(room.room.Position.Y > connection.room.Position.Y)
                {
                    position = new Vector2I(room.room.Position.X + room.room.Size.X / 2, room.room.Position.Y);
                    isVertical = false;
                }

                room.room.Doors.Add(new Door(connection.room, position, isVertical));
            }           
        }              
    }
}