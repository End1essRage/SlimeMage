using Godot;
using System;
using System.Collections.Generic;

public partial class Door
{
    public RoomBase toRoom{ get; set; }
    public Vector2I Position{ get; set; }
    private bool isOpen{ get; set; }
    public bool IsVertical {get;set;}

    public DoorDirection Direction;
    public Door()
    {
    }

    public Door(DoorDirection direction)
    {
        Direction = direction;
    }
    
    public Door(RoomBase ToRoom, Vector2I pos, bool isVertical)
    {
        toRoom = ToRoom;
        isOpen = true;
        Position = pos;
        IsVertical = isVertical;
    }

    public enum DoorDirection
    {
        Top,
        Right,
        Bottom,
        Left
    }
}