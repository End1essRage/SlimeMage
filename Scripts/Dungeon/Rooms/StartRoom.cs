using Godot;
using System;
using System.Collections.Generic;

public partial class StartRoom : RoomBase
{
    public Vector2I spawnPoint;

    public StartRoom(Vector2I size, Vector2I pos) : base(size, pos)
    {
        spawnPoint = Size / 2;
        
        //Add spawn point tile
        TilesPositionAndAtlas[spawnPoint] = ItemsTile.SpawnPoint;
    }
}
