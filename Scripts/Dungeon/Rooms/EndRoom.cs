using Godot;
using System;
using System.Collections.Generic;

public partial class EndRoom : RoomBase
{
    private Vector2 exitPoint;

    public EndRoom(Vector2I size, Vector2I pos) : base(size, pos)
    {
        exitPoint = Size / 2;
    }
}