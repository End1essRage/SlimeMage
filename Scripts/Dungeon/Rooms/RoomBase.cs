using Godot;
using System;
using System.Collections.Generic;

public partial class RoomBase
{
    public Vector2I Size { get; set; }
    public Vector2I Position{get;set;}
    public List<Door> Doors { get; set; } = new List<Door>();
    public Dictionary<Vector2I, Vector2I> TilesPositionAndAtlas { get; set; } = new Dictionary<Vector2I, Vector2I>();

    public RoomBase(Vector2I size, Vector2I pos)
    {
        Size = size;
        Position = pos;
        AddFloorAndWalls();
    }

    private void AddFloorAndWalls()
    {
        //set floor
        for(int i = Position.X; i < Position.X + Size.X; i++)
        {
            for(int j = Position.Y; j < Position.Y + Size.Y; j++)
            {
                TilesPositionAndAtlas.Add(new Vector2I(i,j), ItemsTile.Floor);
            }
        } 
        //set walls
        for(int i = Position.X; i < Position.X + Size.X + 1; i++)
        {
            if(TilesPositionAndAtlas.TryAdd(new Vector2I(i,Position.Y), ItemsTile.Wall) == false){TilesPositionAndAtlas[new Vector2I(i,Position.Y)] = ItemsTile.Wall;};
            if(TilesPositionAndAtlas.TryAdd(new Vector2I(i,Position.Y + Size.Y), ItemsTile.Wall) == false){TilesPositionAndAtlas[new Vector2I(i,Position.Y + Size.Y)] = ItemsTile.Wall;};
        }  
        for(int j = Position.Y; j < Position.Y + Size.Y; j++)
        {
            if(TilesPositionAndAtlas.TryAdd(new Vector2I(Position.X, j), ItemsTile.Wall) == false){TilesPositionAndAtlas[new Vector2I(Position.X, j)] = ItemsTile.Wall;};
            if(TilesPositionAndAtlas.TryAdd(new Vector2I(Position.X + Size.X, j), ItemsTile.Wall) == false){TilesPositionAndAtlas[new Vector2I(Position.X + Size.X, j)] = ItemsTile.Wall;};
        } 
    }
}