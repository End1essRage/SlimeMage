using Godot;
using System;
using System.Collections.Generic;

public partial class Floor
{
    public List<RoomSlot> rooms {get;set;} = new List<RoomSlot>();

    public bool IsActual = false;
    
    public Floor(List<RoomSlot> Rooms)
    {
        rooms.AddRange(Rooms);
    }
}

public struct RoomSlot
{
    public RoomBase room;
    public Vector2I slot;
}