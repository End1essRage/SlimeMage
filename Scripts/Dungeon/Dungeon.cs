using Godot;
using System;
using System.Collections.Generic;

public partial class Dungeon : Node2D
{
    public List<Floor> Floors = new List<Floor>();
    private FloorFactory factory = new FloorFactory();
    private FloorSettings settings = new FloorSettings();
    private TileMap tileMap;
    private Drawer drawer;
    private Player player;
    public Dungeon(int roomsCount, Player _player)
    {
        settings.RoomsCount = roomsCount;
        Floors.Add(factory.CreateFloor(settings));
        Floors[0].IsActual = true;
        player = _player;
    }

    public override void _Ready()
    {
        base._Ready();
        tileMap = new TileMap();
        tileMap.TileSet = ResourceLoader.Load<TileSet>("res://Items/Tilesets/floor_tileset.tres");
        AddChild(tileMap);
        
        drawer = new Drawer(tileMap);
        drawer.DrawFloor(Floors.Find(c => c.IsActual));

        SpawnPlayer();
    }

    public void SpawnPlayer()
    {
        var startRoom = (StartRoom)Floors[0].rooms.Find(r => r.room is StartRoom).room;
        
        AddChild(player);
        player.GlobalPosition = (Vector2)startRoom.spawnPoint * 16.5f;

    }
    //Set actual floor
}