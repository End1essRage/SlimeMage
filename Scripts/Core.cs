using Godot;
using System;

public partial class Core : Node
{
    private Dungeon dungeon;

    public override void _Ready()
    {
        base._Ready();
        var playerScene = GD.Load<PackedScene>("res://Assets/player.tscn");
        Player player = playerScene.Instantiate<Player>();

        dungeon = new Dungeon(60, player);  
        AddChild(dungeon);

        GD.Print("Core ready");  
    }
}
