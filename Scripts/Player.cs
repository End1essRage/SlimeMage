using Godot;
using System;

public partial class Player : CharacterBody2D
{
    private AnimatedSprite2D sprite;
    private CollisionShape2D collisionShape;

    public float Speed = 200f;

    public override void _Ready()
    {
        base._Ready();
        sprite = GetChild<AnimatedSprite2D>(0);
        sprite.Play("Idle_Bottom");
        collisionShape = GetChild<CollisionShape2D>(1);
    }

    public override void _PhysicsProcess(double delta)
    {
        base._PhysicsProcess(delta);

        Vector2 velocity = Velocity;	
        Movement(ref velocity);
        Velocity = velocity;
        MoveAndSlide();
    }

    private void Movement(ref Vector2 velocity)
	{	
		Vector2 direction = new Vector2();
		if(Input.IsActionPressed("MoveLeft"))
		{
			direction.X = -1f;
		}
		else if(Input.IsActionPressed("MoveRight"))
		{
			direction.X = 1f;
		}
		else
		{
			direction.X = 0f;
		}

        if(Input.IsActionPressed("MoveUp"))
		{
			direction.Y = -1f;
		}
		else if(Input.IsActionPressed("MoveDown"))
		{
			direction.Y = 1f;
		}
		else
		{
			direction.Y = 0f;
		}

		if (direction != Vector2.Zero)
		{
			velocity = direction.Normalized() * Speed;
		}
		else
		{
			velocity.X = Mathf.MoveToward(Velocity.X, 0, Speed);
            velocity.Y = Mathf.MoveToward(Velocity.Y, 0, Speed);
		}
	}
}
