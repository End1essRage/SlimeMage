using Godot;
using System;

public partial class Drawer
{
    private TileMap tileMap{get;}
    public Drawer(TileMap map)
    {
        tileMap = map;
    }

    public void DrawFloor(Floor floor)
    {
        var curFloor = floor;
        if(curFloor != null)
        {
            foreach(var room in curFloor.rooms)
            {
                DrawRoom(room.room.Position, room.room);     
            }
        }
    }

    private void DrawRoom(Vector2I position, RoomBase room)
    {
        foreach(var tile in room.TilesPositionAndAtlas)
        {
            tileMap.SetCell(0, tile.Key, 1, tile.Value);
        }

        DrawDoors(room);
    }

    private void DrawDoors(RoomBase room)
    {
        foreach(var door in room.Doors)
        {    
            tileMap.SetCell(0,door.Position, 1,  new Vector2I(23,2));
            if(door.IsVertical)
            {
                tileMap.SetCell(0,new Vector2I(door.Position.X, door.Position.Y + 1), 1,  ItemsTile.Door);
                tileMap.SetCell(0,new Vector2I(door.Position.X, door.Position.Y - 1), 1,  ItemsTile.Door);
            }
            else
            {
                tileMap.SetCell(0,new Vector2I(door.Position.X + 1, door.Position.Y), 1,  ItemsTile.Door);
                tileMap.SetCell(0,new Vector2I(door.Position.X - 1, door.Position.Y), 1,  ItemsTile.Door);
            }
        }
    }
}