using Godot;
using System;

public static class ItemsTile
{
    public static Vector2I Wall = new Vector2I(19,3);
    public static Vector2I Floor = new Vector2I(28,7);
    public static Vector2I Door = new Vector2I(23,2);
    public static Vector2I SpawnPoint = new Vector2I(3,1);
}
